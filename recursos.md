# Recursos generales

## Textos/cursos online

* Libro de física con hyperlinks. [Hyperphysics](http://hyperphysics.phy-astr.gsu.edu/hbase/hframe.html). *En español*   
* Libro de física de [Richard Feynman](http://www.feynmanlectures.caltech.edu/). *En ingles*  
* Recursos de fsica experimental del Prof. Salvador Gil  
  
  [Laboratorio Física 1 ](http://users.df.uba.ar/sgil/unsam/fisica1_usam/experimentos/index_exp.htm). *En español*  
  
  [Física recreativa](http://www.fisicarecreativa.com/) *En español*  

## Simulaciones

* [Phet](https://phet.colorado.edu/es/simulations/category/physics)  

## Física con smartphone

* Sitio dedicado a la utilización de teléfonos celulares para experimentos de física  
  
  [Smarterphysics](http://smarterphysics.blogspot.com/)

## Experimentos básicos con Arduino

* [Sparkfun](https://learn.sparkfun.com/CSC_Sparkfun_001)

## Hardware científico de bajo costo para experimentos de mecánica clásica

* [Repositorio Open Science Framework](https://osf.io/e7nxd/)

# Recursos por tema

## Introducción a la física

* [Historia de la Física por Michio Kaku](https://www.youtube.com/watch?v=QR009W-k5Ps)
* [Mapa de la física](http://www.openculture.com/2016/12/the-map-of-physics.html)

## Metrología

* Presentación de clase y simuladores de micrómetro y calibre. [Metrología](https://gitlab.com/nanocastro/FisicaI_UTN-FRM/tree/master/Metrolog%C3%ADa)
* En su justa medida. Programa canal encuentro. [Qué es medir?](https://www.youtube.com/watch?v=tn_1LR0e_Ps)
