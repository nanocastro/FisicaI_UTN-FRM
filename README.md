# Fisica I UTN-FRM - Comisión 1X100

URL abreviado goo.gl/8Q81Vo

## Sitio de la Cátedra de Física I de la comisión 1X100

En este sitio podrá encontrar las novedades y los recursos para el cursado del año 2020

### Horario de cursado

Lunes de        -> Práctica y laboratorio (con el Prof. Marcelo Rouselle)  

Martes de      -> Teoría  
Miércoles de      -> Teoría  
Miercoles de 17:00 a 18:00 -> Consulta

### Programa y planificación de la materia

En el [Programa de la materia](/Programa Fisica I 2020_2doSemestre.docx.pdf) podrá encontrar el temario, el cronograma anual y las condiciones de cursado y evaluación

### Fechas de los parciales

* Semana 7/9 al 11/9 -> Primera evaluación parcial: Temas 1,2 y 3.
* Semana 12/10 al 16/10 -> Segunda evaluación parcial: Temas 4 al 7.
* Semana 2/11 al 6/11 -> Tercera evaluación parcial: Temas 8 al 11
* Semana 16/11 al 20/11 -> Recuperatorios parciales 1, 2 y 3
* Semana 23/11 al 27/11 -> Global Integrador y entrega de carpetas
* 1° Mesa de final de Física I -> Recuperatorio Global Integrador

### Recursos

* [Guía de trabajos prácticos](https://gitlab.com/nanocastro/FisicaI_UTN-FRM/blob/master/Guia_TP_Fisica_I__2018.pdf)

* [Recursos utilizados o mencionados en clase](https://gitlab.com/nanocastro/FisicaI_UTN-FRM/blob/master/recursos.md)
